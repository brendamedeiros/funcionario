public class Data {
	int dia;
	int mes;
	int ano;
	String formato;
	
	public void setDia (int dia){
		this.dia = dia;
	}

	public int getDia (){
		return dia;
	}

	public void setMes (int mes){
		this.mes = mes;
	}

	public int getMes(){
		return mes;
	}

	public void  setAno(int ano){
		this.ano = ano;
	}

	public int getAno(){
		return ano;
	}

	public String formatada (){ 
		formato = dia+"/"+mes+"/"+ano;
		return formato;
	}
}	
