public class Funcionario {
	private String nome;
	private String departamento;
	private double salario;
	private String RG;
	private double ganhoAnual;
	Data dataEntrada;

	//Metodos acessadores: setters e getters	
	public void setNome (String nome){
		this.nome = nome;
	}

	public String getNome (){
		return this.nome;
	}

	public void setDepartamento (String departamento){
                this.departamento = departamento;
        }

	public String getDepartamento (){
                return this.departamento;
        }
	
	public void setSalario (double salario){
                this.salario = salario;
        }
	
	public double  getSalario (){
                return this.salario;
        }

	public void setRG (String RG){
                this.RG = RG;
        }

	public String getRG (){
                return this.RG;
        }
	
	public void recebeAumento(double aumento){
		this.salario = salario + aumento;
	}

	public double calculaGanhoAnual() {
		ganhoAnual = (this.salario*12);
		return ganhoAnual;
	}

	//Mostrar os dados atualizados referente ao funcionario
	public void mostra(){
        	System.out.println("Nome:" + this.nome);
		System.out.println("Departamento:" + this.departamento);
        	System.out.println("RG:" + this.RG);
	 	System.out.println("Salario atual:" + this.salario);
		
		/*
		System.out.println("Dia: " + this.dataEntrada.dia);
		System.out.println("Mês: " + this.dataEntrada.mes);
		System.out.println("Ano: " + this.dataEntrada.ano);
		*/

		System.out.println("Data de entrada: " + this.dataEntrada.formatada());	
		
	}           

	
}
